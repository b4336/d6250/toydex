pragma solidity >=0.5.0 <0.9.0; 
// pragma solidity ^0.5.0;
// pragma solidity ^0.8.14;

import "./Token.sol";

// This decentralized exchange does the following:
// 0. set the fee account & fee percentage
// 1. deposit Ether (with no mistake)
// 2. withdraw Ether
// 3. deposit tokens
// 4. withdraw tokens
// 5. check balances
// 6. make orders
// 7. cancel orders
// 8. fill orders (i.e. trade)
// 9. charge fees when orders are filled

contract Exchange {
    // variables
    address public feeAccount;
    uint256 public feePercent;
    
    // stores Ether in tokens mapping with blank address
    address constant ETHER = address(0); 

    // the first key specifies what token is referred to;
    // the second key specifies the user holding it
    // the final value specifies how much tokens there are
    mapping(address => mapping(address => uint256)) public tokens; 

    mapping(uint256 => _Order) public orders;
    uint256 public orderCount;
    mapping(uint256 => bool) public ordersCancelled;
    mapping(uint256 => bool) public ordersFilled;

    // struct (for modelling the orders; for internal use only)
    struct _Order {
        uint256 id;         // unique ID for each of the orders
        address user;       // who made the order
        address tokenGet;   // get which token
        uint256 amountGet;  // get how much tokens
        address tokenGive;  // give which token
        uint256 amountGive; // give how much tokens
        uint256 timestamp;  // when the order was made
    }

    // events
    event Deposit(address indexed _token, address indexed _user, uint256 _amount, uint256 _balance);
    event Withdrawal(address indexed _token, address indexed _user, uint256 _amount, uint256 _balance);
    event Order(
        uint256 id,
        address user,
        address tokenGet,
        uint256 amountGet,
        address tokenGive,
        uint256 amountGive,
        uint256 timestamp
    );
    event CancelOrder(
        uint256 id,
        address user,
        address tokenGet,
        uint256 amountGet,
        address tokenGive,
        uint256 amountGive,
        uint256 timestamp
    );
    event FillOrder(
        uint256 id,
        address userCreate,
        address tokenGet,
        uint256 amountGet,
        address tokenGive,
        uint256 amountGive,
        address userFill,
        uint256 timestamp
    );

    // functions
    // fallback: reverts if Ether is sent to this smart contract by mistake
    function() external {
        revert();
    }

    function depositEther() public payable {
        tokens[ETHER][msg.sender] += msg.value; // msg.value is the Ether held by msg.sender
        emit Deposit(ETHER, msg.sender, msg.value, tokens[ETHER][msg.sender]);
    }

    function withdrawEther(uint256 _amount) public {
        require(tokens[ETHER][msg.sender] >= _amount);
        tokens[ETHER][msg.sender] -= _amount;
        msg.sender.transfer(_amount);
        emit Withdrawal(ETHER, msg.sender, _amount, tokens[ETHER][msg.sender]);
    }

    function depositToken(address _token, uint256 _amount) public {
        // _token is passed in to tell the Exchange what token to deposit
        require(_token != ETHER);
        require(Token(_token).transferFrom(msg.sender, address(this), _amount));
        tokens[_token][msg.sender] += _amount;
        emit Deposit(_token, msg.sender, _amount, tokens[_token][msg.sender]);
    }

    function withdrawToken(address _token, uint256 _amount) public {
        require(_token != ETHER);
        require(tokens[_token][msg.sender] >= _amount);
        require(Token(_token).transferFrom(address(this), msg.sender, _amount));
        tokens[_token][msg.sender] -= _amount;
        emit Withdrawal(_token, msg.sender, _amount, tokens[_token][msg.sender]);
    }

    function balanceOf(address _token, address _user) public view returns (uint256) {
        return tokens[_token][_user];
    }

    function makeOrder(address _tokenGet, uint256 _amountGet, address _tokenGive, uint256 _amountGive) public {
        orderCount += 1;
        orders[orderCount] = _Order(orderCount, msg.sender, _tokenGet, _amountGet, _tokenGive, _amountGive, now);
        emit Order(orderCount, msg.sender, _tokenGet, _amountGet, _tokenGive, _amountGive, now);
    }

    function cancelOrder(uint256 _id) public {
        _Order storage _order = orders[_id];
        require(address(_order.user) == msg.sender); // makes sure the same person made and cancels the order
        require(_order.id == _id); // the order must exist
        ordersCancelled[_order.id] = true;
        emit CancelOrder(_order.id, msg.sender, _order.tokenGet, _order.amountGet, _order.tokenGive, _order.amountGive, now);
    }
    
    function fillOrder(uint256 _id) public {
        require(_id > 0 && _id <= orderCount);
        _Order storage _order = orders[_id];
        require(!ordersCancelled[_order.id]);
        require(!ordersFilled[_order.id]);
        require(_order.id == _id); // the order must exist
        _tradeWithFee(_order.id, _order.user, _order.tokenGet, _order.amountGet, _order.tokenGive, _order.amountGive);
        ordersFilled[_order.id] = true;
        emit FillOrder(_order.id, _order.user, _order.tokenGet, _order.amountGet, _order.tokenGive, _order.amountGive, msg.sender, now);
    }

    function _tradeWithFee(uint256 _id, address _user, address _tokenGet, uint256 _amountGet, address _tokenGive, uint256 _amountGive) internal {
        uint256 _feeAmountCreator = _amountGet * feePercent / 100; // fee paid by order creator, i.e. the _user
        uint256 _feeAmountFiller = _amountGive * feePercent / 100; // fee paid by order filler, i.e. the msg.sender
        tokens[_tokenGet][msg.sender] -= _amountGet;
        tokens[_tokenGet][_user] += (_amountGet - _feeAmountCreator);
        tokens[_tokenGet][feeAccount] += _feeAmountCreator; // exchange gets fee from order creator
        tokens[_tokenGive][_user] -= _amountGive;
        tokens[_tokenGive][msg.sender] += (_amountGive - _feeAmountFiller);
        tokens[_tokenGive][feeAccount] += _feeAmountFiller; // exchange gets fee from order filler
    }

    // the constructor
    constructor(address _feeAccount, uint256 _feePercent) public {
        feeAccount = _feeAccount;
        feePercent = _feePercent;
        orderCount = 0;
    }
}