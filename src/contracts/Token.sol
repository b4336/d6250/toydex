// The logic in this smart contract is based on the
// ERC-20 token standard. Details can be found at:
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md

pragma solidity >=0.5.0 <0.9.0; 
// pragma solidity ^0.5.0;
// pragma solidity ^0.8.14;

// import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Token {
    // using SafeMath for uint;

    // variables
    string public name = "Imoan";
    string public symbol = "IMA";
    uint256 public decimals = 18;
    uint256 public totalSupply;
    mapping(address => uint256) public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;

    // events
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

    // functions
    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(_to != address(0));
        require(_value <= balanceOf[msg.sender]);
        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public returns (bool success) {
        require(_spender != address(0));
        allowance[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        require(approve(msg.sender, allowance[_from][msg.sender]));
        require(_to != address(0));
        require(_value <= allowance[_from][msg.sender]);
        require(_value <= balanceOf[_from]);
        allowance[_from][msg.sender] -= _value;
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        emit Transfer(_from, _to, _value);
        return true;
    }

    // the constructor (initialized when the contract is first deployed)
    constructor() public {
        uint256 totalTokens = 100000000; // One hundred million tokens
        totalSupply = totalTokens * (10 ** decimals);
        balanceOf[msg.sender] = totalSupply;
    }
}