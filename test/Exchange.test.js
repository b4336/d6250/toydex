import { tokens, ether, EVM_REVERT, ETHER_ADDRESS } from "./helpers"

require("chai")
    .use(require("chai-as-promised"))
    .should()

const Token = artifacts.require("./Token")
const Exchange = artifacts.require("./Exchange")

contract("Exchange", ([deployer, feeAccount, user1, user2]) => {
    let token
    let exchange
    const feePercent = 10

    beforeEach(async () => {
        // deploys the Token smart contract
        token = await Token.new()

        // transfers some tokens to user1
        token.transfer(user1, tokens(100))

        // deploys the Exchange smart contract
        exchange = await Exchange.new(feeAccount, feePercent)
    })

    describe("Deployment of the Exchange", () => {
        it("tracks the fee account", async () => {
            const result = await exchange.feeAccount()
            result.should.equal(feeAccount)
        })

        it("tracks the fee percent", async () => {
            const result = await exchange.feePercent()
            result.toString().should.equal(feePercent.toString())
        })
    })

    describe("Fallback", () => {
        it("reverts when Ether is sent", async () => {
            await exchange.sendTransaction({ from: user1, value: 1 }).should.be.rejectedWith(EVM_REVERT)
        })
    })

    describe("Depositing Ether", async () => {
        let result
        let amount

        beforeEach(async () => {
            amount = ether(1)
            result = await exchange.depositEther({ from: user1, value: amount })
        })
        
        it("tracks the Ether deposit", async () => {
            const balance = await exchange.tokens(ETHER_ADDRESS, user1)
            balance.toString().should.equal(amount.toString())
        })

        it("emits a Deposit event", async () => {
            // console.log(result)
            const log = result.logs[0]
            log.event.should.equal("Deposit")
            const event = log.args
            event._token.should.equal(ETHER_ADDRESS)
            event._user.should.equal(user1)
            event._amount.toString().should.equal(amount.toString())
            event._balance.toString().should.equal(amount.toString())
        })
    })

    describe("Withdrawing Ether", async () => {
        let result
        let amount

        beforeEach(async () => {
            amount = ether(1)
            await exchange.depositEther({ from: user1, value: amount })
        })

        describe("Success", async () => {
            beforeEach(async () => {
                result = await exchange.withdrawEther(amount, { from: user1 })
            })

            it("withdraws Ether funds", async () => {
                const balance = await exchange.tokens(ETHER_ADDRESS, user1)
                balance.toString().should.equal("0")
            })

            it("emits Withdrawal event", async () => {
                // console.log(result)
                const log = result.logs[0]
                log.event.should.equal("Withdrawal")
                const event = log.args
                event._token.should.equal(ETHER_ADDRESS)
                event._user.should.equal(user1)
                event._amount.toString().should.equal(amount.toString())
                event._balance.toString().should.equal("0")
            })
        })

        describe("Failure", async () => {
            it("rejects withdrawal of Ether for insufficient balance", async () => {
                await exchange.withdrawEther(ether(2), { from: user1 }).should.be.rejectedWith(EVM_REVERT)
            })
        })
    })

    describe("Depositing Tokens", () => {
        let amount1
        let amount2
        let result

        describe("Success", () => {
            beforeEach(async () => {
                amount1 = tokens(100)
                amount2 = tokens(10)
                await token.approve(exchange.address, amount1, { from: user1 })
                result = await exchange.depositToken(token.address, amount2, { from: user1 })
            })

            it("tracks the token deposit", async () => {
                let balance
                // checks the exchange's token balance
                balance = await token.balanceOf(exchange.address)
                balance.toString().should.equal(amount2.toString())
                // checks the balance of user on the exchange
                balance = await exchange.tokens(token.address, user1)
                balance.toString().should.equal(amount2.toString())
            })

            it("emits a Deposit event", async () => {
                // console.log(result)
                const log = result.logs[0]
                log.event.should.equal("Deposit")
                const event = log.args
                event._token.should.equal(token.address)
                event._user.should.equal(user1)
                event._amount.toString().should.equal(amount2.toString())
                event._balance.toString().should.equal(amount2.toString())
            })
        })

        describe("Failure", () => {
            it("rejects deposits of Ether", async () => {
                await exchange.depositToken(ETHER_ADDRESS, amount2).should.be.rejectedWith(EVM_REVERT)
            })

            it("fails when no tokens are approved", async () => {
                await exchange.depositToken(token.address, amount2).should.be.rejectedWith(EVM_REVERT)
            })
        })
    })

    describe("Withdrawing Tokens", () => {
        let result
        let amount

        beforeEach(async () => {
            amount = tokens(1)
            await token.approve(exchange.address, amount, { from: user1 })
            await exchange.depositToken(token.address, amount, { from: user1 })
        })

        describe("Success", async () => {
            beforeEach(async () => {
                result = await exchange.withdrawToken(token.address, amount, { from: user1 })
            })

            it("withdraws token funds", async () => {
                const balance = await exchange.tokens(token.address, user1)
                balance.toString().should.equal("0")
            })

            it("emits Withdrawal event", async () => {
                // console.log(result)
                const log = result.logs[0]
                log.event.should.equal("Withdrawal")
                const event = log.args
                event._token.should.equal(token.address)
                event._user.should.equal(user1)
                event._amount.toString().should.equal(amount.toString())
                event._balance.toString().should.equal("0")
            })
        })

        describe("Failure", async () => {
            it("rejects withdrawal of Ether", async () => {
                await exchange.withdrawToken(ETHER_ADDRESS, amount).should.be.rejectedWith(EVM_REVERT)
            })

            it("rejects withdrawal of the token for insufficient balance", async () => {
                await exchange.withdrawToken(token.address, tokens(2), { from: user1 }).should.be.rejectedWith(EVM_REVERT)
            })
        })
    })

    describe("Checking Balances", async () => {
        beforeEach(async () => {
            await exchange.depositEther({ from: user1, value: ether(1) })
        })

        it("returns the user's balance", async () => {
            const balance = await exchange.balanceOf(ETHER_ADDRESS, user1)
            balance.toString().should.equal(ether(1).toString())
        })
    })

    describe("Making Orders", async () => {
        let result

        beforeEach(async () => {
            result = await exchange.makeOrder(token.address, tokens(1), ETHER_ADDRESS, ether(1), { from: user1 })
        })

        it("tracks the newly created order", async () => {
            const orderCount = await exchange.orderCount()
            orderCount.toString().should.equal("1")
            const order = await exchange.orders("1")
            order.id.toString().should.equal("1", "id is correct")
            order.user.should.equal(user1, "user is correct")
            order.tokenGet.should.equal(token.address, "tokenGet is correct")
            order.amountGet.toString().should.equal(tokens(1).toString(), "amountGet is correct")
            order.tokenGive.should.equal(ETHER_ADDRESS, "tokenGive is correct")
            order.amountGive.toString().should.equal(ether(1).toString(), "amountGive is correct")
            order.timestamp.toString().length.should.be.at.least(1, "timestamp is present")
        })

        it("emits Order event", async () => {
            // console.log(result)
            const log = result.logs[0]
            log.event.should.equal("Order")
            const event = log.args
            event.id.toString().should.equal("1", "id is correct")
            event.user.should.equal(user1, "user is correct")
            event.tokenGet.should.equal(token.address, "tokenGet is correct")
            event.amountGet.toString().should.equal(tokens(1).toString(), "amountGet is correct")
            event.tokenGive.should.equal(ETHER_ADDRESS, "tokenGive is correct")
            event.amountGive.toString().should.equal(ether(1).toString(), "amountGive is correct")
            event.timestamp.toString().length.should.be.at.least(1, "timestamp is present")
        })
    })

    describe("Order Actions", async () => {
        beforeEach(async () => {
            // user1 (i.e. order creator) deposits Ether
            await exchange.depositEther({ from: user1, value: ether(1) })
            
            // user2 (i.e. order filler) is given some tokens, and then deposits tokens with approval
            await token.transfer(user2, tokens(100), { from: deployer })
            await token.approve(exchange.address, tokens(2), { from: user2 })
            await exchange.depositToken(token.address, tokens(2), { from: user2 })
            
            // user1 makes order to buy the tokens with Ether
            await exchange.makeOrder(token.address, tokens(1), ETHER_ADDRESS, ether(1), { from: user1 })
        })

        describe("Cancelling Orders", async () => {
            let result

            describe("Success", async () => {
                beforeEach(async () => {
                    result = await exchange.cancelOrder("1", { from: user1 })
                })
                
                it("updates cancelled orders", async () => {
                    const orderCancelled = await exchange.ordersCancelled(1)
                    orderCancelled.should.equal(true)
                })

                it("emits CancelOrder event", async () => {
                    // console.log(result)
                    const log = result.logs[0]
                    log.event.should.equal("CancelOrder")
                    const event = log.args
                    event.id.toString().should.equal("1", "id is correct")
                    event.user.should.equal(user1, "user is correct")
                    event.tokenGet.should.equal(token.address, "tokenGet is correct")
                    event.amountGet.toString().should.equal(tokens(1).toString(), "amountGet is correct")
                    event.tokenGive.should.equal(ETHER_ADDRESS, "tokenGive is correct")
                    event.amountGive.toString().should.equal(ether(1).toString(), "amountGive is correct")
                    event.timestamp.toString().length.should.be.at.least(1, "timestamp is present")
                })
            })

            describe("Failure", async () => {
                it("rejects invalid order IDs", async () => {
                    const invalidOrderId = 99999
                    await exchange.cancelOrder(invalidOrderId, { from: user1 }).should.be.rejectedWith(EVM_REVERT)
                })

                it("rejects unauthorized cancelation", async () => {
                    await exchange.cancelOrder("1", { from: user2 }).should.be.rejectedWith(EVM_REVERT)
                })
            })
        })

        describe("Filling Orders", async () => {
            let result

            describe("Success", async () => {
                beforeEach(async () => {
                    result = await exchange.fillOrder("1", { from: user2 }) // user2 fills the order
                })

                it("executes the trade and charges fees", async () => {
                    // checks the balance of all relevant accounts
                    let balance
                    // balances of both token accounts
                    balance = await exchange.balanceOf(token.address, user1)
                    balance.toString().should.equal(tokens(0.9).toString(), "user1 received the token with 10% fee deducted")
                    balance = await exchange.balanceOf(token.address, user2)
                    balance.toString().should.equal(tokens(1).toString(), "user2 gave away 1 token")
                    // balances of both Ether accounts
                    balance = await exchange.balanceOf(ETHER_ADDRESS, user1)
                    balance.toString().should.equal(ether(0).toString(), "user1 gave away 1 Ether")
                    balance = await exchange.balanceOf(ETHER_ADDRESS, user2)
                    balance.toString().should.equal(ether(0.9).toString(), "user2 received Ether with 10% fee deducted")
                    // balance of the fee account of the Exchange
                    balance = await exchange.balanceOf(token.address, feeAccount)
                    balance.toString().should.equal(tokens(0.1).toString(), "the Exchange received 0.1 tokens as fee")
                    balance = await exchange.balanceOf(ETHER_ADDRESS, feeAccount)
                    balance.toString().should.equal(ether(0.1).toString(), "the Exchange received 0.1 Ether as fee")
                })

                it("updates filled orders", async () => {
                    const orderFilled = await exchange.ordersFilled(1)
                    orderFilled.should.equal(true)
                })

                it("emits FillOrder event", async () => {
                    // console.log(result.logs[0].args)
                    const log = result.logs[0]
                    log.event.should.equal("FillOrder")
                    const event = log.args
                    event.id.toString().should.equal("1", "id is correct")
                    event.userCreate.should.equal(user1, "user is correct")
                    event.tokenGet.should.equal(token.address, "tokenGet is correct")
                    event.amountGet.toString().should.equal(tokens(1).toString(), "amountGet is correct")
                    event.tokenGive.should.equal(ETHER_ADDRESS, "tokenGive is correct")
                    event.amountGive.toString().should.equal(ether(1).toString(), "amountGive is correct")
                    event.userFill.should.equal(user2, "user who filled the order is correct")
                    event.timestamp.toString().length.should.be.at.least(1, "timestamp is present")
                })
            })

            describe("Failure", async () => {
                it("rejects invalid order ID", async () => {
                    const invalidOrderId = 99999
                    await exchange.fillOrder(invalidOrderId, { from: user2 }).should.be.rejectedWith(EVM_REVERT)
                })

                it("rejects already-filled orders", async () => {
                    await exchange.fillOrder(1, { from: user2 }).should.be.fulfilled
                    await exchange.fillOrder(1, { from: user2 }).should.be.rejectedWith(EVM_REVERT)
                })

                it("rejects cancelled orders", async () => {
                    await exchange.cancelOrder(1, { from: user1 }).should.be.fulfilled
                    await exchange.fillOrder(1, { from: user2 }).should.be.rejectedWith(EVM_REVERT)
                })
            })
        })
    })
})