import { tokens, EVM_REVERT } from "./helpers"
// import { result } from "lodash"

require("chai")
    .use(require("chai-as-promised"))
    .should()

// const { default: Web3 } = require("web3")
const Token = artifacts.require("./Token")

contract("Token", (accounts) => {
    const name = "Imoan"
    const symbol = "IMA"
    const decimals = "18"
    const totalSupply = tokens(100000000).toString() // "100000000000000000000000000" Wei
    let token

    beforeEach(async () => {
        token = await Token.new()   // fetches token from the blockchain
    })

    // 1. Tests for Deployment (name, symbol, decimals and total supply of the token)
    describe("Deployment of the Token", () => {
        it("tracks the name", async () => {
            const result = await token.name() // reads the token name
            result.should.equal(name)         // checks the token name is "Imoan"
        })

        it("tracks the symbol", async () => {
            const result = await token.symbol()
            result.should.equal(symbol)
        })

        it("tracks the decimals", async () => {
            const result = await token.decimals()
            result.toString().should.equal(decimals)
        })

        it("tracks the total supply", async () => {
            const result = await token.totalSupply()
            result.toString().should.equal(totalSupply)
        })

        it("assigns the total supply to the deployer", async () => {
            const result = await token.balanceOf(accounts[0])
            result.toString().should.equal(totalSupply)
        })
    })

    // 2. Tests for Transfer from the Deployer
    describe("Transfering Tokens from Deployer", () => {
        let amount
        let result

        describe("Success", async () => {
            beforeEach(async () => {
                // amount = tokens(50000000)
                amount = tokens(1)
                result = await token.transfer(accounts[1], amount)
            })

            it("transfers tokens", async () => {
                let balance
                balance = await token.balanceOf(accounts[0])
                balance.toString().should.equal(tokens(99999999).toString())
                // console.log("Deployer's balance after transfer: ", balance.toString())
                balance = await token.balanceOf(accounts[1])
                balance.toString().should.equal(amount.toString())
                // console.log("Receiver's balance after transfer: ", balance.toString())
                // result.should.equal(true)
            })

            it("emits a Transfer event", async () => {
                const log = await result.logs[0]
                log.event.should.equal("Transfer")
                const event = await log.args
                event._from.should.equal(accounts[0], "from-address is correct")
                event._to.should.equal(accounts[1], "to-address is correct")
                event._value.toString().should.equal(amount.toString(), "value is correct")
            })
        })

        describe("Failure", async () => {
            it("rejects insufficient balance", async () => {
                let invalidAmount = tokens(200000000) // double the amount available
                await token.transfer(accounts[1], invalidAmount).should.be.rejectedWith(EVM_REVERT)

                // tries to transfer 1 token from accounts[1] (i.e. the original receiver) to accounts[0] (i.e. the deployer of the contract)
                invalidAmount = tokens(1)
                await token.transfer(accounts[0], invalidAmount, { from: accounts[1] }).should.be.rejectedWith(EVM_REVERT)
            })

            it("rejects invalid recipients", async () => {
                await token.transfer(0x0, tokens(1)).should.be.rejected // 0x0 is an invalid address
            })
        })
    })

    // 3. Tests for Approving Delegated Spending
    describe("Approving Delegated Spending", () => {
        let amount
        let result

        beforeEach(async () => {
            amount = tokens(100)
            result = await token.approve(accounts[2], amount)
        })

        describe("Success", async () => {
            it("allocates an allowance for delegated tokens spending", async () => {
                const allowance = await token.allowance(accounts[0], accounts[2])
                allowance.toString().should.equal(amount.toString())
            })

            it("emits an Approval event", async () => {
                const log = await result.logs[0]
                log.event.should.equal("Approval")
                const event = await log.args
                event._owner.should.equal(accounts[0], "owner's address is correct")
                event._spender.should.equal(accounts[2], "spender's address is correct")
                event._value.toString().should.equal(amount.toString(), "value is correct")
            })
        })

        describe("Failure", async () => {
            it("rejects invalid spenders", async () => {
                await token.approve(0x0, amount).should.be.rejected // 0x0 is an invalid address
            })
        })
    })

    // 4. Tests for Trasnfer from Delegated Spender
    describe("Transfering Tokens from Delegated Spender", () => {
        let amount1
        let amount2
        let result1
        let result2

        beforeEach(async () => {
            amount1 = tokens(100)
            result1 = await token.approve(accounts[2], amount1)
        })

        describe("Success", async () => {
            beforeEach(async () => {
                amount2 = tokens(1)
                result2 = await token.transferFrom(accounts[0], accounts[1], amount2, { from: accounts[2] })
            })

            it("transfers tokens", async () => {
                let balance
                balance = await token.balanceOf(accounts[0])
                balance.toString().should.equal(tokens(99999999).toString())
                balance = await token.balanceOf(accounts[1])
                balance.toString().should.equal(amount2.toString())
            })

            it("resets the allowance", async () => {
                let allowance
                allowance = await token.allowance(accounts[0], accounts[2])
                allowance.toString().should.equal(tokens(99).toString())
            })

            it("emits a Transfer event", async () => {
                const log = await result2.logs[1]
                log.event.should.equal("Transfer")
                const event = await log.args
                event._from.should.equal(accounts[0], "from-address is correct")
                event._to.should.equal(accounts[1], "to-address is correct")
                event._value.toString().should.equal(amount2.toString(), "value is correct")
            })
        })

        describe("Failure", async () => {
            it("rejects insufficient allowance", async () => {
                let invalidAmount = tokens(200) // double the allowance approved
                await token.transferFrom(accounts[0], accounts[1], invalidAmount).should.be.rejectedWith(EVM_REVERT)

                // tries to transfer 1 token from accounts[1] (i.e. the original receiver) to accounts[0] (i.e. the deployer of the contract)
                invalidAmount = tokens(1)
                await token.transferFrom(accounts[1], accounts[0], invalidAmount, { from: accounts[2] }).should.be.rejectedWith(EVM_REVERT)
            })

            it("rejects invalid recipients", async () => {
                await token.transferFrom(accounts[0], 0x0, tokens(1)).should.be.rejected // 0x0 is an invalid address
            })
        })
    })
})