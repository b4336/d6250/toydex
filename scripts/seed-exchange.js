// import { tokens, ether, EVM_REVERT, ETHER_ADDRESS } from "./helpers"
// Syntax error: cannot import statement outside a module; hence the following "Utils"

// const { default: Web3 } = require("web3")

const Token = artifacts.require("Token")
const Exchange = artifacts.require("Exchange")

// Utils (very much like helpers.js in the "test" directory)
const ETHER_ADDRESS = "0x0000000000000000000000000000000000000000"
const tokens = (n) => {
    return new web3.utils.BN(
        web3.utils.toWei(n.toString(), "ether")
    )
}
const ether = (n) => {
    return new web3.utils.BN(
        web3.utils.toWei(n.toString(), "ether")
    )
}
const wait = (seconds) => {
    const milliseconds = seconds*1000
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

module.exports = async function(callback) {
    try {
        // fetches the accounts from wallet (unlocked); just like the tests
        const accounts = await web3.eth.getAccounts()
        console.log("The accounts' addresses:", accounts)

        // fetches the deployed smart contract Token
        const token = await Token.deployed()
        console.log("Token smart contract fetched:   ", token.address)

        // fetches the deployed smart contract Exchange
        const exchange = await Exchange.deployed()
        console.log("Exchange smart contract fetched:", exchange.address)

        // transfers some tokens to accounts[1]
        const sender = accounts[0]
        const receiver = accounts[1]
        let amount = web3.utils.toWei("10000", "ether")
        await token.transfer(receiver, amount, { from: sender })
        console.log("Transferred", amount, "tokens from", sender, "to", receiver)

        // sets up user1; deposits Ether
        const user1 = accounts[0]
        amount = 1
        await exchange.depositEther({ from: user1, value: ether(amount) })
        console.log("Deposited", amount, "Ether from", user1)

        // sets up user2; approves and deposits the token
        const user2 = accounts[1]
        amount = 10000
        await token.approve(exchange.address, tokens(amount), { from: user2 })
        console.log("Approved", amount, "tokens from", user2)
        await exchange.depositToken(token.address, tokens(amount), { from: user2 })
        console.log("Deposited", amount, "tokens from", user2)

        // seeds a cancelled order (from user1)
        let result
        let orderId
        result = await exchange.makeOrder(token.address, tokens(100), ETHER_ADDRESS, ether(0.1), { from: user1 })
        console.log(user1, "made an order to buy the token with Ether")
        orderId = result.logs[0].args.id
        await exchange.cancelOrder(orderId, { from: user1 })
        console.log(user1, "cancelled the order")

        // seeds three filled orders (user2 fills orders made by user1)
        result = await exchange.makeOrder(token.address, tokens(100), ETHER_ADDRESS, ether(0.1), { from: user1 })
        console.log(user1, "made an order to buy the token with Ether")
        orderId = result.logs[0].args.id
        await exchange.fillOrder(orderId, { from: user2 })
        console.log(user2, "filled the order")

        await wait(1) // waits one second

        result = await exchange.makeOrder(token.address, tokens(200), ETHER_ADDRESS, ether(0.15), { from: user1 })
        console.log(user1, "made another order to buy the token with (fewer) Ether")
        orderId = result.logs[0].args.id
        await exchange.fillOrder(orderId, { from: user2 })
        console.log(user2, "filled the order")

        await wait(1)  // waits one second again

        result = await exchange.makeOrder(token.address, tokens(50), ETHER_ADDRESS, ether(0.01), { from: user1 })
        console.log(user1, "made the final order to buy the token with (even fewer) Ether")
        orderId = result.logs[0].args.id
        await exchange.fillOrder(orderId, { from: user2 })
        console.log(user2, "filled the final order anyway")

        await wait(1) // waits one more second

        // seeds some open orders (both user1 and user2 make ten open orders each)
        for (let i=1; i<=10; i++) {
            result = await exchange.makeOrder(token.address, tokens(10*i), ETHER_ADDRESS, ether(0.1*i), { from: user1 })
            console.log(user1, "made the", i, "-th order")
            await wait(1)
        }

        for (let i=1; i<=10; i++) {
            result = await exchange.makeOrder(token.address, tokens(10*i), ETHER_ADDRESS, ether(0.1*i), { from: user2 })
            console.log(user2, "made the", i, "-th order")
            await wait(1)
        }

    } catch(error) {
        console.log(error)
    }
    callback()
}